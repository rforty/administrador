package facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Modelo;


//En el modelo creamos la clase del objeto, debe tener los getter y setter de las variables de la clase

public class Buses {

    private int asientos;
    private String hora;


    public Buses() {
    }


    public int getAsientos() {
        return asientos;
    }


    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }


    public String getHora() {
        return hora;
    }


    public void setHora(String hora) {
        this.hora = hora;
    }
}
