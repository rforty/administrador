package facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Vista;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Presentador.Interfaces.FirebaseSuccessListener;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Presentador.TraerDatosFirebase;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.R;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Reservacion;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.UserPojo;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.utils.Constants;


public class MainActivity extends  AppCompatActivity implements FirebaseSuccessListener {


    //Referencia Database
    DatabaseReference mDatabase;
    private int checkSuma;

    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Buses"); //referencia al nodo padre de nuestra firebase
        TraerDatosFirebase tdf = new TraerDatosFirebase (this);// creamos objeto de la clase TraerDatosFirebase
        tdf.setInterfaz(this);
        tdf.getDatosFirebase(mDatabase);//asignamos nuestra referencia mDatabase al metodo que obtiene los datos
    }



    //metodo implementado por la interface que comprueba si los dos datos fueron obtenidos de la firebase
    @Override
     public void onDatosBajados(int bajado) {
        checkSuma +=  bajado;
        if(checkSuma ==Constants.DATA_DESCARGA_COMPLETA){
            Toast.makeText(this, "Se termino de bajar los datos. ", Toast.LENGTH_SHORT).show();

        }

    }
}
