package facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Presentador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Modelo.Buses;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Presentador.Interfaces.FirebaseSuccessListener;
import facci.ronny_forty.creacionyeliminaciondedatosconfirebase.utils.Constants;


//objetivo de la arquitectura MVP: minimizar el numero de lineas de codigo en la vista
public class TraerDatosFirebase {

    //variable estatica para ver los datos obtenidos de la Firebase en el Logcat
    private static final String TAG = "TraerDatosFirebase";

    //variable del constructor de esta clase
    Context mContext;

    //variable de la interface
    private FirebaseSuccessListener mSuccessListener;


    //constructor
    public TraerDatosFirebase(Context mContext) {
        this.mContext = mContext;
    }

    //metodo para que el MainActivty escuche los cambios que va a tener ese listener
    public void setInterfaz(FirebaseSuccessListener interfaz) {
        this.mSuccessListener = interfaz;
    }

    //metodo que trae los datos de firebase
    public void getDatosFirebase(DatabaseReference mDatabase) {


        //addListenerForSingleValueEvent  realiza la consulta de datos una sola vez
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Buses bus = snapshot.getValue(Buses.class);
                    assert bus != null;
                    int asientos = bus.getAsientos();
                    //String hora = bus.getHora();
                    Log.i(TAG, "onDataChangeAsientos: " + asientos);
                    //Log.i(TAG, "onDataChangeHora: " + hora);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Buses bus = snapshot.getValue(Buses.class);
                    assert bus != null;
                    String hora = bus.getHora();
                    Log.i(TAG, "onDataChangeHora: " + hora);
                }
                //metodo de interfaz escucha si se realizo el onDataChange
                mSuccessListener.onDatosBajados(Constants.DATA_2_DESCARGADO);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        mSuccessListener.onDatosBajados(Constants.DATA_1_DESCARGADO);


    }


}
