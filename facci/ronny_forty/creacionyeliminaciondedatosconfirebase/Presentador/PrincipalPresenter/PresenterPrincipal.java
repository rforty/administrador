package facci.ronny_forty.creacionyeliminaciondedatosconfirebase.Presentador.PrincipalPresenter;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class PresenterPrincipal {

    private Context mContext;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public PresenterPrincipal(Context mContext, DatabaseReference mDatabase, FirebaseAuth mAuth) {
        this.mContext = mContext;
        this.mDatabase = mDatabase;
        this.mAuth = mAuth;
    }
}