package facci.ronny_forty.creacionyeliminaciondedatosconfirebase;

import android.support.annotation.NonNull;
import android.support.v13.view.DragStartHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class Reservacion extends AppCompatActivity {

    private TextView DatoObtenido, HoraObtenida;
    private DatabaseReference mDatabase;
    private Button buttonReservar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservacion);



        DatoObtenido = (TextView)findViewById(R.id.tv_dato);
        HoraObtenida = (TextView)findViewById(R.id.tv_dato2);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        buttonReservar = (Button)findViewById(R.id.btn1_rsvar_uni);

        mDatabase.child("BUSES DISPONIBLES").child("Bus disponible").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String dato = dataSnapshot.child("Nro Asientos").getValue().toString();
                    String hora =dataSnapshot.child("Hora de partida").getValue().toString();
                    DatoObtenido.setText(" ASIENTOS DISPONIBLES: "+ dato );
                    HoraObtenida.setText("HORA DE PARTIDA:" + hora);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });

        buttonReservar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDatabase.child("BUSES DISPONIBLES").child("Bus disponible").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                           String F_Dato = dataSnapshot.child("Nro Asientos").getValue().toString();
                            int num_dato = Integer.parseInt(F_Dato);
                            int resta = num_dato -1;
                            //String F_Asiento = String.valueOf(resta);

                            Map<String, Object > AsientosMap = new HashMap<>();
                            AsientosMap.put("Nro Asientos",resta);
                            mDatabase.child("BUSES DISPONIBLES").child("Bus disponible").updateChildren(AsientosMap);

                        }
                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }


        });





    }

}
