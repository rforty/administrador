package facci.ronny_forty.creacionyeliminaciondedatosconfirebase;

public class UserPojo {


    private  int asientos;
    private String hora;

    public UserPojo() {

    }

    public int getAsientos() {
        return asientos;
    }

    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
